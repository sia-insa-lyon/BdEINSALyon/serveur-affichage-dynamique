# Affichage Dynamique (Digital Signage)
Application d'affichage dynamique utilisée sur le campus de l'INSA Lyon (LyonTech-la Doua)

## Env variables

```
ENV DATABASE_URL postgres://ad@db/ad
ENV SECRET_KEY ''
ENV DJANGO_ENV 'dev'
ENV MAILJET_API_KEY ''
ENV MAILJET_SECRET_KEY ''
ENV DEFAULT_FROM_EMAIL ''
ENV EMAIL_SUBJECT_PREFIX '[Affichage Dynamique]'
ENV DEFAULT_GROUP_PK 0 #Pk du groupe ajouté automatiquement à chaque nouveau inscrit
ENV ALLOWED_HOSTS "affichage.bde-insa-lyon.fr"
ENV HCAPTCHA_SITEKEY ''
ENV HCAPTCHA_SECRET ''
ENV API_EVENTS ''
ENV ADMIN_EMAIL ''
ENV SITE_URL "https://affichage.bde-insa-lyon.fr"
ENV CONTACT_MAIL ''
ENV MENTION_ROLE 'Président'
ENV MENTION_MAIL ''
ENV MENTION_PHONE ''
```

## Install

Juste use `docker-compose` command to set up the app. You can get HCaptcha key/secret by creating an account on their [site](https://www.hcaptcha.com/).

**Important** : if there is no user group yet, set `DEFAULT_GROUP_PK` to 0.

## Cron job command

If you have a doubt regarding the cron job that are executed, you can run `python3 manage.py crontab show` or `python3 manage.py crontab -l`.

If there is no cron job in response, you can run `python3 manage.py crontab add` to add them again, and then run the previous command to check if cron jobs are active.

In case you cannot wait for the cron job to run, you can manually trigger it by getting the id of the cron job from `python3 manage.py crontab -l`, and then using `/usr/local/bin/python3 /app/manage.py crontab run <id>`

## Raspberry py clients config

Configure the static IP of the VPS
```bash
sudo nmcli c mod "Wired connection 1" ipv4.addresses 134.214.129.27/24 ipv4.method manual
sudo nmcli con mod "Wired connection 1" ipv4.gateway 134.214.129.1
```

You can run the installation script by running :
```bash
bash <(curl -s https://gitlab.com/sia-insa-lyon/BdEINSALyon/serveur-affichage-dynamique/-/raw/master/scripts_raspberry/install.sh?ref_type=heads)
```

### Install packages and create kiosk user

```bash
# Create kiosk user (requires user input)
sudo adduser kiosk
sudo usermod -aG sudo kiosk
# Install and update packages
sudo apt update
sudo apt upgrade -y
sudo apt install -y python3 python3-pip libcec-dev build-essential git matchbox-window-manager x11-xserver-utils unclutter xinit chromium
```

### Register the screen token
Type this command to get the screen token that will be used to register the screen on the Affichage Dynamique server :
```bash
md5sum /etc/machine-id | awk '{print $1;}'
```

### Copy script files

Download the required scripts and files to the raspberry pi :
```bash
sudo mkdir /kiosk && cd /kiosk
sudo git init
sudo git remote add -f origin https://gitlab.com/sia-insa-lyon/BdEINSALyon/serveur-affichage-dynamique.git
sudo git config core.sparseCheckout true
sudo echo "scripts_raspberry/*" | sudo tee .git/info/sparse-checkout
sudo git pull origin master
sudo mv ./scripts_raspberry/* /kiosk/
sudo rm -R scripts_raspberry/
sudo chmod +x /kiosk/*.sh
```
Install venv and python packages :
```bash
sudo python -m venv /kiosk/venv
sudo /kiosk/venv/bin/pip install -r /kiosk/requirements.txt
```

### Startup and autologin

Configure the raspberry pi to start the x server and the startup script at startup :
```bash
sudo su kiosk
echo "nohup /kiosk/venv/bin/python /kiosk/monitoring_rpi.py >/dev/null 2>&1 &" >> ~/.profile
echo "if [ -z \"\${SSH_TTY}\" ]; then" >> ~/.profile
echo "    startx" >> ~/.profile
echo "fi" >> ~/.profile
echo "/kiosk/start.sh" >> ~/.xinitrc
```

Setup autologin, read-only file system, and reboot
```bash
sudo su kiosk
sudo raspi-config
````
- 1 System Options > S5 Boot / Auto Login > B2 Console Autologin
- 4 Performance Options > P2 Overlay File System > Yes
- Select Finish, and reboot the Raspberry Pi.

## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
AffichageDynamique - Open Source Digital Signage used in INSA Lyon School of Engineering (France).
Copyright (C) 2019 Romain TORRENTE
Copyright (C) 2021 SIA INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
