import os
import requests
import urllib.request
from datetime import datetime
from django.contrib.auth import get_user_model
from django.core.mail import send_mail, mail_admins
from django.template.loader import render_to_string
from django.utils import timezone

from AffichageDynamique import settings
from .models import Image, Content, Feed, Screen
from .utils import resize_img

User = get_user_model()


def delete_image_orphan():
    list = os.listdir(settings.MEDIA_ROOT + "/contents")
    image = Image.objects.all()
    imagelist = []
    for img in image:
        string = img.image.name
        imagelist.append(string.replace("contents/", ""))
    for img in list:
        if img not in imagelist:
            os.remove(settings.MEDIA_ROOT + "/contents/" + img)


def delete_old_content():
    date = timezone.now() - timezone.timedelta(days=7)
    content = Content.objects.filter(end_date__lt=date)
    content.delete()
    content = Content.objects.filter(state="R").filter(begin_date__lt=date)
    content.delete()
    content = Content.objects.filter(is_valid=False).filter(submission_date__lt=date)
    content.delete()


def delete_old_user():
    date = timezone.now() - timezone.timedelta(days=365)
    user_list = (
        User.objects.filter(last_login__lt=date)
        .filter(is_active=True)
        .filter(is_superuser=False)
    )
    for user in user_list:
        date_supression = (user.last_login + timezone.timedelta(days=395)).date()
        if timezone.now().date() <= date_supression:
            msg_plain = render_to_string(
                "app/email_inactive.txt",
                {
                    "user": user,
                    "date_supression": date_supression,
                    "site": settings.ALLOWED_HOSTS[0],
                },
            )
            send_mail(
                settings.EMAIL_SUBJECT_PREFIX + " Compte inactif",
                msg_plain,
                settings.DEFAULT_FROM_EMAIL,
                [user.email],
            )
        else:
            user.delete()
    date_inactive = timezone.now() - timezone.timedelta(
        days=(settings.ACCOUNT_ACTIVATION_DAYS + 2)
    )
    User.objects.filter(date_joined__lt=date_inactive).filter(is_active=False).filter(
        is_superuser=False
    ).delete()


def notify_moderation():
    last = timezone.now() - timezone.timedelta(hours=12)
    feed_list = (
        Feed.objects.filter(content_feed__state="P", content_feed__is_valid=True)
        .filter(date_last_moderation_email__lt=last)
        .distinct()
    )
    for feed in feed_list:
        user_list = User.objects.filter(groups=feed.moderator_group).distinct()
        for user in user_list:
            msg_plain = render_to_string(
                "app/email_pending_moderation.txt",
                {"user": user, "site": settings.ALLOWED_HOSTS[0], "feed": feed},
            )
            send_mail(
                settings.EMAIL_SUBJECT_PREFIX
                + " "
                + feed.name
                + " - Contenu à modérer",
                msg_plain,
                settings.DEFAULT_FROM_EMAIL,
                [user.email],
            )
        feed.date_last_moderation_email = timezone.now()
        feed.save()


def notify_screen_hs():
    """
    Fonction appelée tout les jours à 6h de septembre à juin.
    Listage de l'ensemble des écrans, pour chaque écran s'il est HS
    on l'ajoute dans la liste des écrans HS qui sera envoyé par mail
    aux settings.ADMINS. Nous mettont par ailleur à jour la
    date_last_problem_email de l'écran.
    """
    screen_list = Screen.objects.all()
    screen_list_hs = []

    for screen in screen_list:
        if not screen.is_ok:
            screen_list_hs.append(screen)
            screen.date_last_problem_email = timezone.now()
            screen.save()

    if len(screen_list_hs) > 0:
        msg_plain = render_to_string(
            "app/email_screen_hs.txt", {"screen_list": screen_list_hs}
        )
        mail_admins("Problème sur {} écran(s)".format(len(screen_list_hs)), msg_plain)


def maj_event_pva(current_user=None):
    if current_user is None:
        current_user = User.objects.get(username=settings.DEFAULT_USERNAME_EVENT)
    # Acquisition de la liste des évènements sur portail VA
    events = requests.get(settings.API_EVENTS).json()
    current_tz = timezone.get_current_timezone()
    # Pour chaque évènement, si ce dernier à du contenu à afficher créé ou met à jour le contenu en question
    list_id = []
    for event in events:
        if "poster" in event["external"].keys():
            list_id.append(event["id"])
            defaults = {
                "name": event["name"],
                "begin_date": timezone.make_aware(
                    datetime.strptime(
                        event["external"]["begin_publication_at"], "%Y-%m-%dT%H:%M:%S"
                    ),
                    current_tz,
                ),
                "end_date": timezone.make_aware(
                    datetime.strptime(
                        event["external"]["end_publication_at"], "%Y-%m-%dT%H:%M:%S"
                    ),
                    current_tz,
                ),
                "state": "A",
                "is_valid": True,
                "feed": Feed.objects.get(name="Affichage"),  # Le flux des associations
                "duration": event["external"]["duration"],
                "user": current_user,
                "user_moderator": current_user,
            }
            content, created = Content.objects.update_or_create(
                eventId=event["id"], defaults=defaults
            )

            # Création ou MàJ de l'objet 'image' associé
            url_image = event["external"]["poster"]
            image, headers = urllib.request.urlretrieve(url_image)
            resize_img(image, content)

    # Suppression des contenus s'il ne sont plus dans l'API
    contents = Content.objects.exclude(eventId__exact=None)
    # Nous prenons uniquement les contenus de PVA (donc ayant un eventId)
    for content in contents:
        if content.eventId not in list_id:
            content.delete()


# Set exportable var for manually triggering some cron job method
cron_job_available_name = [
    "Supprimer les images sans contenu",
    "Supprimer le contenu expiré depuis au moins 7 jours",
    "Supprimer les utilisateurs non-connectés depuis 365 jours",
    "Synchroniser le contenu avec Portail VA",
]

cron_job_available = [
    delete_image_orphan,
    delete_old_content,
    delete_old_user,
    maj_event_pva,
]
