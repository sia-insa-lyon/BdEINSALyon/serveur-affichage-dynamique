from django.conf import settings


def app_settings(request):
    """
    A context processor with all APP settings.
    """
    return {
        "app": settings.AFFICHAGE_APP,
        "repository": settings.AFFICHAGE_APP["site"]["repository"],
        "editor": settings.AFFICHAGE_APP["site"]["editor"],
        "site_contact": settings.AFFICHAGE_APP["site"]["contact_mail"],
        "site_url": settings.SITE_URL,
    }
