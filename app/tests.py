from django.test import TestCase
from app.models import HourGroup
from app.templatetags.app_filters import anonymise_mail


# TODO Add real tests
class HourGroupTestCase(TestCase):
    def setUp(self):
        HourGroup.objects.create(name="GroupeTest")

    def test_hourgroup_can_be_recorded(self):
        """HourGroup can be recorded"""
        test = HourGroup.objects.get(name="GroupeTest")
        self.assertEqual(str(test), "GroupeTest")


class ObfuscatingMailTestCase(TestCase):
    """This class defines the test suite for the mail obfuscation method."""

    def test_generate_valid_mail(self):
        self.assertEqual(
            anonymise_mail("test@test.com", False),
            '&#116;&#101;&#115;&#116;<span style="display:none">bde</span>&#064;<span style="display:none">insa</span>&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;<span style="display:none">ancebde.fr</span>',
            "Valid mail is not properly obfuscated",
        )

    def test_generate_valid_mailto(self):
        self.assertEqual(
            anonymise_mail("test@test.com", True),
            "&#116;&#101;&#115;&#116;&#064;&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;",
            "Valid mailto is not properly obfuscated",
        )

    def test_generate_mail_ignore_unknown_char(self):
        self.assertEqual(
            anonymise_mail("tést@test.com", False),
            '&#116;é&#115;&#116;<span style="display:none">bde</span>&#064;<span style="display:none">insa</span>&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;<span style="display:none">ancebde.fr</span>',
            "Unknown char in mail are not properly obfuscated",
        )

    def test_generate_mailto_ignore_unknown_char(self):
        self.assertEqual(
            anonymise_mail("tést@test.com", True),
            "&#116;é&#115;&#116;&#064;&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;",
            "Unknown char in mailto are not properly obfuscated",
        )
