upstream proxy_app {
    server app:8000;
}
server {
  listen 8080;
  server_name localhost;

  # remember about this line!
  server_tokens off;
  client_max_body_size 20M;
  add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
  add_header Content-Security-Policy "frame-ancestors 'none'";
  add_header X-XSS-Protection "1; mode=block";
  add_header Permissions-Policy "interest-cohort=()";

  location /static/ {
    autoindex off;
    alias /app/staticfiles/;
  }

  location /media/ {
    autoindex off;
    alias /app/media/;
  }

  location /healthcheck {
    add_header Access-Control-Request-Method "OPTIONS, GET";
    add_header Access-Control-Allow-Origin "*";
    return 200;
  }

  location / {
    proxy_pass http://proxy_app;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;
    proxy_redirect off;
  }
}