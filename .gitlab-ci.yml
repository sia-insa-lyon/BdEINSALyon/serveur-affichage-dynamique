include:
  - template: SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

variables:
  SAST_EXCLUDED_PATHS: app/migrations/**, bash/**, scripts/**, static/**, venv/**
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        DOCKER_TAG: "MR_$CI_MERGE_REQUEST_IID"
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      changes:
        - AffichageDynamique/**/*
        - app/**/*
        - templates/**/*
        - Dockerfile
        - requirements.txt
        - .gitlab-ci.yml
        - sonar-project.properties
      variables:
        DOCKER_TAG: "$CI_COMMIT_REF_NAME.$CI_COMMIT_SHORT_SHA"
    - if: '$CI_COMMIT_BRANCH == "master"'
      variables:
        DOCKER_TAG: "master"


code_quality:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

secret_detection:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

gemnasium-python-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

eslint-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

bandit-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

nodejs-scan-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

semgrep-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'


stages:
  - lint
  - build
  - test
  - metrics

black:
  image: python:3.7
  stage: lint
  before_script:
    - pip install black
  script:
    - black --target-version py37 --check app/ AffichageDynamique/ --exclude /migrations/

helm:
  image: dtzar/helm-kubectl
  stage: lint
  script:
    - helm lint helm/

build:
  image: docker:latest
  stage: build
  needs:
    - job: black
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$DOCKER_TAG --tag $CI_REGISTRY_IMAGE:$DOCKER_TAG .
    - docker push $CI_REGISTRY_IMAGE:$DOCKER_TAG
  services:
    - docker:dind

test:
  image: $CI_REGISTRY_IMAGE:$DOCKER_TAG
  stage: test
  services:
    - postgres:10
  variables:
    DATABASE_URL: postgres://ad@postgres/ad
    POSTGRES_HOST_AUTH_METHOD: trust
    POSTGRES_USER: ad
    POSTGRES_DB: ad
    SECRET_KEY: test
    GIT_STRATEGY: none
  before_script:
    - cd /app
    - pip install pipenv
    - pipenv install -r requirements.txt
    - pipenv install coverage pytest-django
    - pipenv run python manage.py migrate
  script:
    - pipenv run coverage run --source=AffichageDynamique,app --omit=*/migrations/** -m pytest --junitxml=$CI_PROJECT_DIR/output/junit.xml
    - pipenv run coverage report
    - pipenv run coverage xml -o $CI_PROJECT_DIR/output/coverage.xml
  coverage: /^TOTAL.+?(\d+\%)$/
  artifacts:
    expire_in: 1 week
    paths:
      - output
    reports:
      junit:
        - output/junit.xml
      coverage_report:
        coverage_format: cobertura
        path: output/coverage.xml

sonarcloud-check:
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  stage: metrics
  needs:
    - job: test
  dependencies:
    - test
  before_script:
    - "[ -f ./output/coverage.xml ] || (echo 'Coverage report not found' && exit 1)"
    - "[ -f ./output/junit.xml ] || (echo 'Test report not found' && exit 1)"
  script:
    - sonar-scanner
  only:
    - master
    - merge_requests
