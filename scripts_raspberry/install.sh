#!/bin/bash
# Register screen token
echo "Here is the screen token, take note of it to register the screen on Affichage Dynamique admin panel :"
md5sum /etc/machine-id | awk '{print $1;}'
echo "Press any key to continue..."
read -r -n 1 -s
# Create kiosk user (requires console input)
sudo adduser kiosk
sudo usermod -aG sudo kiosk
# Update and install packages
sudo apt update
sudo apt upgrade -y
sudo apt install -y python3 python3-pip libcec-dev build-essential git matchbox-window-manager x11-xserver-utils unclutter xinit chromium
# Copy files
sudo mkdir /kiosk && cd /kiosk || exit
sudo git init
sudo git remote add -f origin https://gitlab.com/sia-insa-lyon/BdEINSALyon/serveur-affichage-dynamique.git
sudo git config core.sparseCheckout true
sudo echo "scripts_raspberry/*" | sudo tee .git/info/sparse-checkout
sudo git pull origin master
sudo mv ./scripts_raspberry/* /kiosk/
sudo rm -R scripts_raspberry/
sudo chmod +x /kiosk/*.sh
sudo echo "https://affichage.bde-insa-lyon.fr" | sudo tee /boot/config_domain.txt
# Create venv and install dependencies
sudo python -m venv /kiosk/venv
sudo /kiosk/venv/bin/pip install -r /kiosk/requirements.txt
# Setup management service
sudo mv /kiosk/monitoring.service /etc/systemd/system/monitoring.service
sudo systemctl start monitoring
sudo systemctl enable monitoring
# Setup X11 and chromium start
sudo su kiosk
# shellcheck disable=SC2129
echo "if [ -z \"\${SSH_TTY}\" ]; then" >> ~/.profile
echo "    startx" >> ~/.profile
echo "fi" >> ~/.profile
echo "/kiosk/start.sh" >> ~/.xinitrc

# Config autologin, readonly fs and reboot
sudo raspi-config