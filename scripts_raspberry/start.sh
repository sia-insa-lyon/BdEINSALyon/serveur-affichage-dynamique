#!/usr/bin/env bash
# disable DPMS (Energy Star) features.
xset -dpms
# disable screen saver
xset s off
# don't blank the video device
xset s noblank
# disable mouse pointer
unclutter &
# run window manager
matchbox-window-manager -use_cursor no -use_titlebar no  &
# run browser
SCREEN_TOKEN=`md5sum /etc/machine-id | awk '{print $1;}'`
DOMAIN=`cat /boot/config_domain.txt | awk '{print $1;}'`
url=${DOMAIN}'/display/'${SCREEN_TOKEN}
chromium-browser --load-extension=/kiosk/h264ify-master --purge-memory-button --smooth-scrolling --no-pings --disable-background-mode --dns-prefetch-disable --ignore-gpu-blacklist --enable-gpu-rasterization --enable-native-gpu-memory-buffers --enable-lazy-image-loading --enable-lazy-frame-loading --enable-checker-imaging --enable-quic --enable-resource-prefetch --enable-tcp-fast-open --disable-gpu-compositing --enable-fast-unload --enable-experimental-canvas-features --enable-scroll-prediction --enable-scroll-anchoring --enable-tab-audio-muting --disable-background-video-track --enable-simple-cache-backend --answers-in-suggest --ppapi-flash-path=/usr/lib/chromium-browser/libpepflashplayer.so --ppapi-flash-args=enable_stage_stagevideo_auto=0 --ppapi-flash-version= --max-tiles-for-interest-area=512 --num-raster-threads=4 --default-tile-height=512 --noerrdialogs --disable-session-crashed-bubble --disable-infobars --disk-cache-size=180000000 --kiosk --no-first-run $url
echo "Chrome crash, reboot dans 120 sec, pour laisser le temps de se co en ssh si bootloop"
sleep 120
sudo reboot
