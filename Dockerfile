FROM python:3.7
EXPOSE 8000

RUN apt-get update && apt-get install -y ghostscript imagemagick libfreetype6 cron && apt-get clean
#RUN sed -i '/PDF/d' /etc/ImageMagick-6/policy.xml
RUN sed -i 's|<policy domain="coder" rights="none" pattern="PDF" />|<policy domain="coder" rights="read" pattern="PDF" />|' /etc/ImageMagick-6/policy.xml
WORKDIR /app
COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
RUN mkdir -p /app/tmp
ENV DATABASE_URL postgres://ad@db/ad
ENV SECRET_KEY ''
ENV DJANGO_ENV 'dev'
ENV MAILGUN_KEY ''
ENV MAILGUN_DOMAIN ''
ENV DEFAULT_FROM_EMAIL ''
ENV DEFAULT_GROUP_PK 0
ENV ALLOWED_HOSTS "affichage.bde-insa-lyon.fr"
ENV HCAPTCHA_SITEKEY ''
ENV HCAPTCHA_SECRET ''
ENV RESTAURANTS_FEED_PK ''
ENV RESTAURANTS_GROUP_PK ''
ENV EMAIL_SUBJECT_PREFIX '[Affichage Dynamique]'
RUN chmod +x bash/run-prod.sh
CMD bash/run-prod.sh
